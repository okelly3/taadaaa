import connexion
import uuid
from google.cloud import datastore

from config import Config
from task import Task


def add_task():
    try:
        new_task = connexion.request.get_json()
        datastore_client = datastore.Client()
        id = str(uuid.uuid4())
        task_key = datastore_client.key("Task", id)
        task = datastore.Entity(key=task_key)
        task["title"] = new_task["title"]
        task["is_complete"] = False
        datastore_client.put(task)
        print(task)
        return Task(id=id, title=new_task["title"], is_complete=False), 200
    except:
        return None, 500


def delete_task(taskId):
    try:
        datastore_client = datastore.Client()
        task_key = datastore_client.key("Task", taskId)
        datastore_client.delete(task_key)
        return None, 200
    except:
        return None, 500


def list_all():
    datastore_client = datastore.Client()
    query = datastore_client.query(kind='Task')

    res = query.fetch()
    list = []
    for task in res:
        print(task)
        list.append(Task(id=task.key.name, title=task["title"], is_complete=task["is_complete"]))
    return list, 200


def toggle_status(taskId):
    try:
        datastore_client = datastore.Client()
        task_key = datastore_client.key("Task", taskId)
        task = datastore_client.get(task_key)
        task["is_complete"] = not task["is_complete"]
        datastore_client.put(task)
        return None, 200
    except:
        return None, 404
        
def add_headers(response):
    """  set response headers  """
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'POST, GET, DELETE, PUT, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type, Access-Control-Allow-Headers, Authorization'
    return response

app = connexion.FlaskApp(__name__, debug=Config.DEBUG)
app.add_api('todo-openapi3.yaml')
app.app.after_request(add_headers)
if __name__ == '__main__':
    app.run(host=Config.HOST, port=Config.PORT)
