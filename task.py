class Task(dict):
    def __init__(self, id: str = None, title: str = None, is_complete: bool = False):
        dict.__init__(self, id=id, title=title, is_complete=is_complete)
