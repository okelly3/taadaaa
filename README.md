##	Instructions on how to deploy to App Engine

** Follow this walkthrough to complete all the next steps: **
	https://walkthroughs.googleusercontent.com/tutorial/iframe-angular.html/?walkthrough_id=python_gae_quickstart

** Enable App Engine in a Cloud project and clone this repo using: **

		create a project
		git clone https://okelly3@bitbucket.org/okelly3/taadaaa.git

** Create and activate virtualenv: **

		virtualenv --python python3 ~/envs/taadaaa
		source ~/envs/taadaaa/bin/activate
	
** Test the app before deploying it: **

		cd taadaaa
		python main.py
		make sure you set the port number in config.py to the same one you choose when you use "Web preview"
		add "/ui" at the end of the URL to explore the microservice methods using SwaggerUI
	

** Deploy the app **

		gcloud config set project [your-project-id]
		gcloud app deploy
		
		add "/ui" at the end of the URL (https://<your-project>.appspot.com/ui) to explore the microservice methods using SwaggerUI
		make sure billing is enabled for the project
		
		
** Running Sample **
	available here:	https://vcita-317308.ue.r.appspot.com/ui